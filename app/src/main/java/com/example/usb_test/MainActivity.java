package com.example.usb_test;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.CellInfo;
import android.telephony.CellInfoLte;
import android.telephony.CellSignalStrength;
import android.telephony.CellSignalStrengthLte;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Text;

import static java.lang.System.exit;
import static java.lang.Thread.sleep;

/*
*NOTE BEFORE YOU RUN THIS:
* When you run the application first time it might fail and hence just open setting give all the permissions listed there.
* Also File training file should exist in the phone storage in the same directory as given below.
*  */

public class MainActivity extends AppCompatActivity  {
    private Timer mUpdateTimer,mUpdateTimer_values;
    String display_devices,web_RSRP,web_SNR,web_BAND;
    BufferedWriter p_stdin;
    ProcessBuilder builder = new ProcessBuilder( "/system/bin/sh" );
    Process p=null;
    String TAG="sandy";
    boolean initialise=true,web_rsrp=false,web_band=false,web_snr=false;
    public int phone_rsrp,phone_rsrq,phone_snr,predicted_class,estimated_load,predicted_load,estimated_class,dongle_rsrp=0,dongle_rsrq=0,dongle_snr=0,dongle_predicted_class,dongle_estimated_load,dongle_predicted_load,dongle_estimated_class;
    public Double dongle_rsrp_double=0.0,dongle_rsrq_double=0.0;
    int ip1=0,ip2=0;
    private static HashMap<String, String> list = new HashMap<>();
    Thread sensing;

    /*Converting the IP address into decimal format*/
    public long ipToLong(String ipAddress) {
        long result = 0;
        String str[] = ipAddress.split("\\.");
        int limit = str.length;
        for (int i = 0; i < limit; i++) {
            int power = 3 - i;
            int ip = Integer.valueOf(str[i]);
            result += ip * Math.pow(256, power);
        }
        return result;
    }

    /*
    Function that predict the load class to which the cell environment belongs to?
     */
    private static int castle_get_best_class(int snr,int rsrp,int rsrq){
        int encoded_value;
        int SINR_class;
        int count;
        //Log.d("castle_library","in predict class\n SNR:\t"+snr+"\nRSRP\t"+rsrp+"\nRSRQ" +"\t"+rsrq+"\n");
        //Log.d("castle_library","in predict class SNR from shared:\t"+lib_sharedpreference.getInt("snr_value",0));
        // decide SINR_class = {0, 1, 2}
        if (snr < 110) {
            SINR_class = 0;
            count = snr / 10;
        } else if (snr < 220) {
            SINR_class = 1;
            count = (snr / 10) - 11;
        } else {
            SINR_class = 2;
            count = (snr / 10) - 22;
        }
        if(count<=0)count=1;
        String find1 = Integer.toString(rsrp);
        String find2 = Integer.toString(rsrq);
        String find3 = Integer.toString(SINR_class);

        String find = find1 + find2 + find3;
        String hash1 = MD5(find);
        System.out.println("get hash code"+hash1);
        if(list.containsKey(hash1)) {
            String load_class = list.get(hash1).trim();
            encoded_value=Integer.valueOf(load_class);
            //Log.d("library","got the key\n"+encoded_value+"count value\t"+count);
            // decode
            while (count > 0)
            {
                encoded_value = encoded_value / 4;
                count--;
            }
            encoded_value = encoded_value % 4;
            //Log.d("library","class by encoding\n"+encoded_value);
            if (encoded_value == 0) {
                return 4;
            }
            else {
                return encoded_value;   // class 1, 2, 3
            }
        }
        else {
            System.out.println("Sorry class entry is not present in the given hash keys");
            return 0;
        }
    }

    private static String MD5( String md5){
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    private static void castle_train_data() {

        String file_path = "/storage/emulated/0/optimised_2.csv";
        //Log.d("Path", "" + file_path);

            File file = new File(file_path);
            if (file.exists()) {
                try {
                    BufferedReader reader = new BufferedReader(new FileReader(file_path));
                    String line;
                    int count = 1;
                    while ((line = reader.readLine()) != null) {
                        String str[] = line.split(",");
                        String md5 = "";
                        int limit = str.length;
                        for (int i = 0; i < limit - 1; i++) {
                            //System.out.println("*****"+str[i]+"*****");
                            md5 = md5 + str[i].trim();
                        }
                        //Log.d("String comb","\n"+md5);
                        String hash_key = MD5(md5);
                        //Log.d("Hash value",""+hash_key+"\n"+count);
                        list.put(hash_key, str[limit - 1]);
                        count++;
                    }
                    reader.close();
                    Log.d("Total entry", "" + count + "Hash map size:" + list.size());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Log.d("CASTLE LIBRARY:", "Data File doesn't exists and please load it");
                exit(0);
            }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Train data to get the castle load
        castle_train_data();
        //Phone cellular sensing and predicting the class

        //Get the ip address and assign path number p2:rmnet_data0 for cellular and p1 for dongle etho0
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if(intf.getName().toLowerCase().contains("rmnet_data0")){
                    List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                    for (InetAddress addr : addrs) {
                        if (!addr.isLoopbackAddress() && addr instanceof Inet4Address) {
                            String sAddr = addr.getHostAddress().toUpperCase();
                            Log.d(TAG,sAddr);
                            long ip=ipToLong(sAddr);
                            while(ip>2147483647){
                                ip=ip-2147483647;
                            }
                            ip1=(int)ip;
                            Log.d(TAG,"IP in decimal:"+ip);
                            TextView str_cellip = (TextView) findViewById(R.id.str_cellip);
                            str_cellip.setText(sAddr);
                            TextView dec_cellip=(TextView)findViewById(R.id.dec_cellip);
                            dec_cellip.setText(""+ip);
                        }
                    }
                }else if(intf.getName().toLowerCase().contains("eth0")){
                    List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                    for (InetAddress addr : addrs) {
                        if (!addr.isLoopbackAddress() && addr instanceof Inet4Address) {
                            String sAddr = addr.getHostAddress().toUpperCase();
                            Log.d(TAG,sAddr);
                            long ip=ipToLong(sAddr);
                            while(ip>2147483647){
                                ip=ip-2147483647;
                            }
                            ip2=(int)ip;
                            Log.d(TAG,"IP in decimal:"+ip);
                            TextView str_dongleip = (TextView) findViewById(R.id.str_dongleip);
                            str_dongleip.setText(sAddr);
                            TextView dec_dongleip=(TextView)findViewById(R.id.dec_dongleip);
                            dec_dongleip.setText(""+ip);
                        }
                    }
                }

            }
        } catch (SocketException ex) {
            Log.d(TAG, ex.toString());
        }

        final TelephonyManager mTelephonyManager=(TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);

        final PhoneStateListener mPhoneStateListener=new PhoneStateListener(){
            public void onSignalStrengthsChanged(SignalStrength signalStrength) {
                try {
                    super.onSignalStrengthsChanged(signalStrength);

                    Pattern SPACE_STR = Pattern.compile(" ");

                    String[] splitSignals = SPACE_STR.split(signalStrength.toString());
                    if (splitSignals.length < 3) {
                        splitSignals = signalStrength.toString().split("[ .,|:]+");
                    }
                    String s_rsrp = splitSignals[9];
                    String s_rsrq = splitSignals[10];
                   // String s_snr = splitSignals[11];//This works with Xifinity Sim card and also change the phone to Moto it works. So I have hard coded now
                    String s_snr ="13";
                    phone_snr = Math.abs(Integer.valueOf(s_snr));
                    phone_rsrp = Math.abs(Integer.valueOf(s_rsrp));
                    phone_rsrq = Math.abs(Integer.valueOf(s_rsrq));
                    Log.d("Cellular SNR: ", "" + phone_snr + " cellular RSRP " + phone_rsrp + "cellular RSRQ" + phone_rsrq);
                    //Compute the cell load now using RSRQ and RSRP
                    estimated_load=((phone_rsrq-6)*100)/(13-6);
                    estimated_class=(estimated_load/25)+1;

                    //predict cell load :
                    predicted_class=castle_get_best_class(phone_snr,-1*phone_rsrp,-1*phone_rsrq);
                    Log.d("sandy","Predicted class: "+predicted_class);
                    if(predicted_class==0){
                        Log.d("sandy","Sorry class cannot be predicted\n\n");
                        exit(0);
                    }else{
                        Log.d("sandy","Predicated class for the given values: "+Integer.toString(predicted_class));
                    }


                    predicted_load=estimated_load;
                    if(estimated_class<predicted_class){
                        if ((predicted_class - estimated_class) > 1)
                            predicted_load+= 20;
                        else
                            predicted_load+= 10;
                    }else {
                        if((estimated_load-predicted_class) > 1)
                            predicted_load-= 20;
                        else
                            predicted_load-= 10;
                    }
                    if(predicted_load<0)    predicted_load=0;
                    else if(predicted_load>100) predicted_load=100;
                    Log.d("sandy","Estimated load for cellular: "+estimated_load+" Predicated load for cellular: "+predicted_load);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };



        sensing= new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
                    mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
                    try{
                        sleep(1000);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
        sensing.setPriority(8);
        sensing.start();

        Thread snr= new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    try{

                            //TextView text_rsrp;
                            // Create a URL for the desired page
                            URL url = new URL("http://my.usb/diagnostics/"); //My text file location
                            //First open the connection
                            HttpURLConnection conn=(HttpURLConnection) url.openConnection();
                            conn.setConnectTimeout(60000); // timing out in a minute

                            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                            String str;
                            while ((str = in.readLine()) != null) {
                                //Log.d("sandy",str);
                                //readings+="#"+str;
                                if(str.contains("dBm")){
                                    Log.d("RSRP",str);
                                    web_RSRP=str;
                                }
                                if(str.contains("internetStatusSNR")&& !(web_snr)){
                                    web_snr=true;
                                }else if(web_snr){
                                    web_SNR=str.substring(1,str.length()).trim();
                                    web_snr=false;
                                }else if(str.contains("lteBand") && !(web_band)){
                                    web_band=true;
                                }else if(web_band){
                                    web_BAND=str;
                                    web_band=false;
                                }
                            }
                            Log.d("sandy","WEB SNR *** "+web_SNR+" ***");
                            if(web_SNR.length()>0)dongle_snr=Integer.valueOf(web_SNR);
                            in.close();
                            //text_rsrp = (TextView) findViewById(R.id.rsrp);
                            //text_rsrp.setText("RSRP:"+web_RSRP);

                            sleep(1000);
                        }catch (Exception e){
                            Log.d("sandy",e.getCause().toString());
                        }
                }
            }
        });

        snr.setPriority(6);
        snr.start();




        /*Thread to read the signal values from AT commands*/
        TimerTask task_getvalues = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String dongle_s_rsrp="",dongle_s_rsrq="";
                        //Get the super user privilege,run AT commands and parse the strings
                        try {

                            p = builder.start();
                            p_stdin = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));
                            //get su permission
                            p_stdin.write("su");
                            p_stdin.newLine();
                            p_stdin.flush();
                            //set the bus baud rate
//                            if(ip1>0 && ip2 >0){
//                                String command=null;
//                                if(dongle_predicted_load>predicted_load) { //p1 load>p2 load p1:Dongle and p2: Cellular
//                                    command = "sysctl -w net.ipv4.tcp_sandy_ip=" + ip2;
//                                }else{
//                                    command = "sysctl -w net.ipv4.tcp_sandy_ip=" + ip1;
//                                }
//                                p_stdin.write(command);
//                                p_stdin.newLine();
//                                p_stdin.flush();
//                            }
                            p_stdin.write("stty -F /dev/ttyUSB0 9600 raw -echo");
                            p_stdin.newLine();
                            p_stdin.flush();
                            //Get RSRQ
                            p_stdin.write("cat < /dev/ttyUSB0 & /system/bin/echo \"AT+VZWRSRQ?\r\n\" > /dev/ttyUSB0");
                            p_stdin.newLine();
                            p_stdin.flush();
                            //Get the RSRP
                            p_stdin.write("cat < /dev/ttyUSB0 & /system/bin/echo \"AT+VZWRSRP?\r\n\" > /dev/ttyUSB0");
                            p_stdin.newLine();
                            p_stdin.flush();
                            // Exit su
                            p_stdin.write("exit");
                            p_stdin.newLine();
                            p_stdin.flush();
                            // Exit the shell
                            p_stdin.write("exit");
                            p_stdin.newLine();
                            p_stdin.flush();


                            Scanner s = new Scanner(p.getInputStream());
                            String output = "";
                            String temp;
                            while (s.hasNextLine()) {
                                temp = s.nextLine().trim();
                                if (temp.length() > 0) {
                                    output += "#" + temp;
                                }
                            }
                            s.close();
                            p_stdin.close();
                            //Log.d("sandy:", output);


                            //Processing the string to display RSRP and RSRQ:

                            String[] results = output.split("#");
                            int len = results.length, index;

                            for (int i = 0; i < len; i++) {
                                String temp1 = results[i];
                                //RSRP

                                if (temp1.contains("+VZWRSRP:")) {
                                    String[] all = temp1.split(":");
                                    String values = all[1];
                                    //Log.d("sandy", values);
                                    String[] cell_earfcn_rsrp = values.split(",");
                                    dongle_s_rsrp=cell_earfcn_rsrp[2];
                                    dongle_s_rsrp= dongle_s_rsrp.substring(1,dongle_s_rsrp.length()-1);
                                    //Log.d("sandy","Dongle RSRP"+dongle_s_rsrp);

                                }

                                //RSRQ

                                if (temp1.contains("+VZWRSRQ:")) {
                                    String[] all = temp1.split(":");
                                    String values = all[1];
                                    //Log.d("sandy",values);
                                    String[] cell_earfcn_rsrq = values.split(",");
                                    dongle_s_rsrq=cell_earfcn_rsrq[2];
                                    dongle_s_rsrq= dongle_s_rsrq.substring(1,dongle_s_rsrq.length()-1);
                                    //Log.d("sandy","Dongle RSRQ"+dongle_s_rsrq);

                                }
                            }

                        } catch (IOException e) {
                            Log.d("sandy",e.getMessage());
                        }
                        if(dongle_s_rsrp.length()>0) dongle_rsrp_double=Math.abs(Double.parseDouble(dongle_s_rsrp));
                        if(dongle_s_rsrq.length()>0) dongle_rsrq_double=Math.abs(Double.parseDouble(dongle_s_rsrq));

                        dongle_rsrp=dongle_rsrp_double.intValue();
                        dongle_rsrq=dongle_rsrq_double.intValue();

                        Log.d("sandy", "Dongle RSRP: " + dongle_rsrp + "Dongle RSRQ: " + dongle_rsrq+"Dongle SNR: "+dongle_snr);
                        //Compute the cell load now using RSRQ and RSRP
                        dongle_estimated_load=((dongle_rsrq-6)*100)/(13-6);
                        dongle_estimated_class=(dongle_estimated_load/25)+1;

                        //predict cell load :
                        dongle_predicted_class=castle_get_best_class(dongle_snr,-1*dongle_rsrp,-1*dongle_rsrq);
                        Log.d("sandy","Dongle Predicted class: "+dongle_predicted_class);
                        if(dongle_predicted_class==0){
                            Log.d("sandy","Sorry class cannot be predicted \n\n");
                            //exit(0);
                        }

                        dongle_predicted_load=dongle_estimated_load;
                        if(dongle_estimated_class<dongle_predicted_class){
                            if ((dongle_predicted_class - dongle_estimated_class) > 1)
                                dongle_predicted_load+= 20;
                            else
                                dongle_predicted_load+= 10;
                        }else {
                            if((dongle_estimated_load-dongle_predicted_class) > 1)
                                dongle_predicted_load-= 20;
                            else
                                dongle_predicted_load-= 10;
                        }
                        if(dongle_predicted_load<0)    dongle_predicted_load=0;
                        else if(dongle_predicted_load>100) dongle_predicted_load=100;
                        Log.d("sandy","Estimated load for Dongle: "+Integer.toString(dongle_estimated_load)+" Predicated load for Dongle: "+Integer.toString(dongle_predicted_load));
                        Log.d("sandy","******* Cellular load"+predicted_load+" Dongle Load"+dongle_predicted_load+"******");
                        TextView celllload = (TextView) findViewById(R.id.cellload);
                        celllload.setText(""+predicted_load);
                        TextView dongleload=(TextView)findViewById(R.id.dongleload);
                        dongleload.setText(""+dongle_predicted_load);
                    }
                });
            }
        };
        mUpdateTimer_values = new Timer();
        mUpdateTimer_values.schedule(task_getvalues, 10000, 1000);
    }

    @Override
    public void onBackPressed(){
        new AlertDialog.Builder(this).setMessage("Exit or Background")
                .setPositiveButton("Exit", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mUpdateTimer.cancel();
                        mUpdateTimer_values.cancel();
                        finish();
                    }
                });

        return;
    }
}
